function list = List()

% stimulus duration: stimOff-saccOn.
% this is because the stimulus duration 
% is calculated from the moment the eye lands.

list = eis_readData([], 'x');
list = eis_readData(list, 'y');

list = eis_readData(list, 'trigger', 'blink');
list = eis_readData(list, 'trigger', 'notrack');

% list = eis_readData(list, 'stream', 0, 'double'); %%
% list = eis_readData(list, 'stream', 1, 'double'); %%

        
%----------------------------------------------------------
%Test Calibration
list = eis_readData(list, 'uservar','TestCalibration');
list = eis_readData(list, 'uservar','xoffset');
list = eis_readData(list, 'uservar','yoffset');

%time of the response (locked to the start of the trial)		
list = eis_readData(list, 'uservar','TimeCueON');
list = eis_readData(list, 'uservar','TimeFixationON');
list = eis_readData(list, 'uservar','TimeTargetON');
list = eis_readData(list, 'uservar','TimeMaskON');

%duration of events (ms)		
list = eis_readData(list, 'uservar','FixationTime');
list = eis_readData(list, 'uservar','TargetTime');
list = eis_readData(list, 'uservar','CueTime');
list = eis_readData(list, 'uservar','MaskTime');
list = eis_readData(list, 'uservar','BeepDelayTime');
list = eis_readData(list, 'uservar','DelayTime');

% response
list = eis_readData(list, 'uservar','Correct');

% target orientation
list = eis_readData(list, 'uservar','Target1Orientation');
list = eis_readData(list, 'uservar','Target2Orientation');
list = eis_readData(list, 'uservar','Target3Orientation');
list = eis_readData(list, 'uservar','Target4Orientation');
list = eis_readData(list, 'uservar','Target5Orientation');
list = eis_readData(list, 'uservar','Target6Orientation');
list = eis_readData(list, 'uservar','Target7Orientation');
list = eis_readData(list, 'uservar','Target8Orientation');
% list = eis_readData(list, 'uservar','TargetFixOrientation');
list = eis_readData(list, 'uservar','Target9Orientation');

list = eis_readData(list, 'uservar','InhibitionTest');
% target size px
list = eis_readData(list, 'uservar','TSizeY');
list = eis_readData(list, 'uservar','TSizeX');

% target offset
list = eis_readData(list, 'uservar','XlocationPx');
list = eis_readData(list, 'uservar','YlocationPx');

% target rgb color
list = eis_readData(list, 'uservar','TargetRGB');

% stimuli size
list = eis_readData(list, 'uservar','FixSize');
list = eis_readData(list, 'uservar','BoxSize');

% cue location
list = eis_readData(list, 'uservar','CueLocation');
list = eis_readData(list, 'uservar','Response');	
list = eis_readData(list, 'uservar','ResponseTime');
list = eis_readData(list, 'uservar','TargetOffsetpx'); % px

% Time saccadic cue is on
list = eis_readData(list, 'uservar','SaccCueTime');
% 0 neutral, 1 R and 2 L
list = eis_readData(list, 'uservar','SaccCueType');

% monitor variables
list = eis_readData(list, 'uservar','RefreshRate');
list = eis_readData(list, 'uservar','Xres');
list = eis_readData(list, 'uservar','Yres');

list = eis_readData(list, 'uservar','Subject_Name');
%----------------------------------------------------------
