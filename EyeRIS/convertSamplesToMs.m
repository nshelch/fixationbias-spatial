function [vt] = convertSamplesToMs(vt, conversionFactor)
% Converts the data gathered from the dDPI, which is stored in samples, to
% miliseconds based on the conversionFactor defined by:
% conversionFactor = refresh rate / 1000

f = fieldnames(vt{1}.microsaccades);
f{strcmp('start',f)} = 'startSample';
f{strcmp('duration',f)} = 'durationSample';

fnames = {'analysis','blinks','drifts','fixations','invalid','microsaccades', ...
    'notracks','saccades'};

for ii = 1:length(vt)
    for fieldnameIdx = 1:length(fnames)
        currentField = fnames{fieldnameIdx};
        tmp = struct2cell(vt{ii}.(currentField));
        vt{ii}.(currentField) = cell2struct(tmp,f);
        vt{ii}.(currentField).start = vt{ii}.(currentField).startSample * conversionFactor;
        vt{ii}.(currentField).duration = vt{ii}.(currentField).durationSample * conversionFactor;
    end
end

end
