clc; clear; %close all;

% NS: 0 -> 90
% Z031: 75
% Z042: 75
% Z066: 50 -> 50
% Z095: 50

Subject = {'Z073','Z042','Z031','Z066'};
rgb_value = [90, 75, 75, 50];
MaxMSaccAmp = 30;
DDPI = 0;

for ii = 1:length(Subject)
    
    if DDPI
        pt = sprintf('./UnprocessedData/%s/RGB%i-DDPI/', Subject{ii}, rgb_value(ii));
        data = readdata2(pt, List);
        vt = preprocessingDDPI(data, MaxMSaccAmp, 330);
        vt = convertSamplesToMs(vt, 1000/330);
        vt = cellfun(@(x) setfield(x, 'pxAngle', 0.6848), vt, 'UniformOutput', false);
        save(sprintf('../Data/%s_RGB%i-DDPI.mat', Subject{ii}, rgb_value(ii)), 'vt')
    else
        pt = sprintf('./UnprocessedData/%s/RGB%i/', Subject{ii}, rgb_value(ii));
        %     pt = sprintf('./UnprocessedData/%s/NeutralOnlyNoSaccCue/', Subject{ii});
        data = readdata2(pt, List);
        vt = preprocessing(data, MaxMSaccAmp);
        vt = cellfun(@(x) setfield(x, 'pxAngle', 0.6848), vt, 'UniformOutput', false);
        save(sprintf('../Data/%s_RGB%i.mat', Subject{ii}, rgb_value(ii)), 'vt')
        %     save(sprintf('../Data/%s_NeutralOnlyNoSaccCue.mat', Subject{ii}), 'vt')
    end
    
end

