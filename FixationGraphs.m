% Graphs for the condition in which a subject remains at the central box
% and can respond to any location
% radius: 20.538

pngFilepath = sprintf('./Documents/Figures/%s_RGB%i/PNG/', params.Subject{sub_idx}, params.RGB(sub_idx));
epscFilepath = sprintf('./Documents/Figures/%s_RGB%i/EPSC/', params.Subject{sub_idx}, params.RGB(sub_idx));
figFilepath = sprintf('./Documents/Figures/%s_RGB%i/FIG/', params.Subject{sub_idx}, params.RGB(sub_idx));

% Distance From Target
plot_distance_from_target(ft, 0, 1)
saveFigure('DistanceFromTarget', {pngFilepath, epscFilepath, figFilepath}, {'png','epsc','fig'})

% Sensitivity heatmaps
trial_types = {'neutral_1', 'neutral_2', 'neutral_3', 'neutral_4', 'neutral_5', ...
    'neutral_6', 'neutral_7', 'neutral_8', 'neutral_9'};
[~, ~, dprime_neutral_vector, ~, neutral_counter] = trialTypeLoop(trial_types, ft, stats);

plotDprimeMap(dprime_neutral_vector, neutral_counter, sprintf('%s Sensitivity at Fixation', params.Subject{sub_idx}))
saveFigure('Dprime', {pngFilepath, epscFilepath, figFilepath}, {'png','epsc','fig'})

plotDprimeHeatmap(dprime_neutral_vector, sprintf('%s Sensitivity at Fixation', params.Subject{sub_idx}))
saveFigure('DprimeHeatmap', {pngFilepath, epscFilepath, figFilepath}, {'png','epsc','fig'})

plotDprimeInterpHeatmap(dprime_neutral_vector, sprintf('%s Sensitivity at Fixation', params.Subject{sub_idx}))
saveFigure('DprimeInterpHeatmap', {pngFilepath, epscFilepath, figFilepath}, {'png','epsc','fig'})

% Response Time heatmap
respTime = [ft.neutral_1.avg_resp_time, ft.neutral_2.avg_resp_time, ft.neutral_3.avg_resp_time, ...
ft.neutral_4.avg_resp_time, ft.neutral_5.avg_resp_time, ft.neutral_6.avg_resp_time, ...
ft.neutral_7.avg_resp_time, ft.neutral_8.avg_resp_time, ft.neutral_9.avg_resp_time];
plotDprimeMap(respTime, neutral_counter, sprintf('%s Resp Time at Fixation', params.Subject{sub_idx}))
cbh = colorbar;
cbh.TickLabels = round(linspace(floor(min(respTime)), ceil(max(respTime)), 11));
ylabel(cbh, 'Avg. Resp Time');
saveFigure('ResponseTime', {pngFilepath, epscFilepath, figFilepath}, {'png','epsc','fig'})

% Bar graph of dprime values
figure;
theta = linspace(0, 2*pi, 9); % angle (theta) vector
trial_types = {'neutral_3', 'neutral_2', 'neutral_1', 'neutral_8', 'neutral_7', ...
    'neutral_6', 'neutral_5', 'neutral_4'};
[~, ~, dprimeModNeutral, ci_dprimeModNeutral, ~] = trialTypeLoop(trial_types, ft, stats);
loDprime = dprimeModNeutral - ci_dprimeModNeutral; hiDprime = dprimeModNeutral + ci_dprimeModNeutral;
[xlo1, ylo1] = pol2cart(theta, [loDprime, loDprime(1)]);
[xhi1, yhi1] = pol2cart(theta, [hiDprime, hiDprime(1)]);
plr3 = polar(theta, [dprimeModNeutral, dprimeModNeutral(1)], 'k');
plr3.LineWidth = 2;
hold all;
hf1 = nan(length(xlo1)-1, 1);
for jj = 1:(length(xlo1)-1)
    hf1(jj) = fill([xlo1(jj:jj+1) xhi1(jj+1:-1:jj)], [ylo1(jj:jj+1) yhi1(jj+1:-1:jj)],...
        'k');
end
set(hf1, 'FaceAlpha', .3, 'EdgeColor', 'none');
set(hf1, 'FaceColor', [.7 .7 .7]);
box off;
title(sprintf('%s Sensitivity', params.Subject{sub_idx}))
saveFigure('DprimeLinePlot', {pngFilepath, epscFilepath, figFilepath}, {'png','epsc','fig'})

% Diffusion Coefficient in x and y
dcoefX = [ft.neutral_1.avg_dcoef_x; ft.neutral_2.avg_dcoef_x; ft.neutral_3.avg_dcoef_x; ...
    ft.neutral_4.avg_dcoef_x; ft.neutral_5.avg_dcoef_x; ft.neutral_6.avg_dcoef_x; ...
    ft.neutral_7.avg_dcoef_x; ft.neutral_8.avg_dcoef_x; ft.neutral_9.avg_dcoef_x]';
dcoefY = [ft.neutral_1.avg_dcoef_y; ft.neutral_2.avg_dcoef_y; ft.neutral_3.avg_dcoef_y; ...
    ft.neutral_4.avg_dcoef_y; ft.neutral_5.avg_dcoef_y; ft.neutral_6.avg_dcoef_y; ...
    ft.neutral_7.avg_dcoef_y; ft.neutral_8.avg_dcoef_y; ft.neutral_9.avg_dcoef_y]';
deltaDcoef = dcoefX - dcoefY;
neutralColors = parula(9);
figure;
hold on
n1 = plot(-10, 10, 'Color', neutralColors(1,:), 'Marker','.','MarkerSize',20, 'LineStyle','none');
n2 = plot(-10, 10, 'Color', neutralColors(2,:), 'Marker','.','MarkerSize',20, 'LineStyle','none');
n3 = plot(-10, 10, 'Color', neutralColors(3,:), 'Marker','.','MarkerSize',20, 'LineStyle','none');
n4 = plot(-10, 10, 'Color', neutralColors(4,:), 'Marker','.','MarkerSize',20, 'LineStyle','none');
n5 = plot(-10, 10, 'Color', neutralColors(5,:), 'Marker','.','MarkerSize',20, 'LineStyle','none');
n6 = plot(-10, 10, 'Color', neutralColors(6,:), 'Marker','.','MarkerSize',20, 'LineStyle','none');
n7 = plot(-10, 10, 'Color', neutralColors(7,:), 'Marker','.','MarkerSize',20, 'LineStyle','none');
n8 = plot(-10, 10, 'Color', neutralColors(8,:), 'Marker','.','MarkerSize',20, 'LineStyle','none');
n9 = plot(-10, 10, 'Color', neutralColors(9,:), 'Marker','.','MarkerSize',20, 'LineStyle','none');
scatter(dprime_neutral_vector, deltaDcoef, 50, neutralColors, 'filled')
axis([floor(min(dprime_neutral_vector)) - .5, ceil(max(dprime_neutral_vector)) + .5, floor(min(deltaDcoef)) - .5, ceil(max(deltaDcoef)) + .5])
plot([-10 10],[-10 10], 'k--')
legend([n1, n2, n3, n4, n5, n6, n7, n8, n9], ...
    {'Neutral 1', 'Neutral 2', 'Neutral 3', 'Neutral 4', 'Neutral 5', 'Neutral 6', 'Neutral 7', 'Neutral 8', 'Neutral 9'}, ...
    'Location','EastOutside')
xlabel("Sensitivity [d']")
ylabel({'\Delta Drift Coefficient'; 'Dcoef X - Dcoef Y'},'interpreter','tex')
saveFigure('driftCoefXYCorrelation', {pngFilepath, epscFilepath, figFilepath}, {'png','epsc','fig'})

% Radial Histogram of drift velocity
figure('units', 'normalized', 'outerposition', [0 0 1 1])
load('./MyColormaps.mat')
for ii = 1:9
    plotLabel = [8,1,2,7,9,3,6,5,4];
    curplot = plotLabel(ii);
    subplot(3,3,ii)
    curTrial = sprintf('neutral_%i',curplot);
    [edgesX2, edgesY2, N] = ndhist(cell2mat(ft.(curTrial).drift.inst_vel_x), cell2mat(ft.(curTrial).drift.inst_vel_y),'bins', 1.25, 'radial','axis',[-150 150 -150 150],'nr','filt');
    title( sprintf('Neutral %i',curplot),'FontSize',12)
    set(gcf, 'Colormap', mycmap)
    shading interp
end
saveFigure('driftInstVelRadialMap', {pngFilepath, epscFilepath, figFilepath}, {'png','epsc','fig'})

figure('position',[300 300 1250 500]);
subplot(1,3,1)
ndhist(cell2mat(cellfun(@(x) single(x), ft.fixation.drift.inst_vel_x, 'UniformOutput',0)), ...
cell2mat(cellfun(@(x) single(x), ft.fixation.drift.inst_vel_y, 'UniformOutput',0)), ...
'bins', .5, 'radial','axis',[-150 150 -150 150],'nr','filt');
title('Fixation','FontSize',15)
load('./MyColormaps.mat')
set(gcf, 'Colormap', mycmap)
shading interp
% Drift Direction Radial Hist
subplot(1,3,2)
plotEmTuningCurve(ft.fixation.drift.angle);
hold all
plotEmTuningCurve(ft.neutral.drift.angle, 'color', 'r');
title('Inst. Drift Velocity', 'FontSize', 15)
subplot(1,3,3)
plotEmTuningCurve(ft.fixation.ms_angle);
title('Ms Angle During Fixation', 'FontSize', 15)
supertitle(sprintf('%s Fixation EM', params.Subject{sub_idx}))
saveFigure('FixationTaskEmComparison', {pngFilepath, epscFilepath, figFilepath}, {'png','epsc','fig'})

% DPrime difference between central and highest location
trial_types = {'neutral_1', 'neutral_2', 'neutral_3', 'neutral_4', 'neutral_5', ...
    'neutral_6', 'neutral_7', 'neutral_8', 'neutral_9'};
[~, ~, dprime_neutral_vector, ci_dprime_neutral_vector, ~] = trialTypeLoop(trial_types, ft, stats);
dprimeVec = [dprime_neutral_vector(9), max(dprime_neutral_vector(1:8))];
dprimeCi = [ci_dprime_neutral_vector(9), max(ci_dprime_neutral_vector(1:8))];
figure;
errorbar(1:2, dprimeVec, dprimeCi, 'ok', 'LineWidth', 2, 'MarkerFaceColor', 'w', 'MarkerSize', 10)
axis([.5 2.5 floor(min(dprimeVec - dprimeCi)), ceil(max(dprimeVec + dprimeCi))])
set(gca, 'FontSize', 12, 'XTick', 1:2, 'XTickLabel', {'Central Loc.','Peripheral Loc.'})
ylabel("Sensitivity [d']")
title(sprintf('%s Highest Lowest Sensitivity', params.Subject{sub_idx}), 'FontSize', 15)
saveFigure('dprimeDiffCentralNasalLocation', {pngFilepath, epscFilepath, figFilepath}, {'png','epsc','fig'})

% Microsaccade Angle
plotEmTuningCurve(ft.fixation.ms_angle);
title('Ms Angle During Fixation')
saveFigure('MicrosaccadeTuningCurve', {pngFilepath, epscFilepath, figFilepath}, {'png','epsc','fig'})

% Dcoef
figure('position',[800 300 800 500])
dcoefVectorFix = [ft.fixation.drift.dcoef, ft.fixation.drift.dcoef_x, ft.fixation.drift.dcoef_y];
semDcoefVectorFix = [ft.fixation.drift.sem_dcoef, ft.fixation.drift.sem_dcoef_x, ft.fixation.drift.sem_dcoef_y];
dcoefVectorNeutral = [ft.neutral.drift.dcoef, ft.neutral.drift.dcoef_x, ft.neutral.drift.dcoef_y];
semDcoefVectorNeutral = [ft.neutral.drift.sem_dcoef, ft.neutral.drift.sem_dcoef_x, ft.neutral.drift.sem_dcoef_y];
errorbar(1:3, dcoefVectorFix, semDcoefVectorFix, 'ok', 'LineWidth', 2, 'MarkerFaceColor', 'w', 'MarkerSize', 10)
hold on
errorbar(1:3, dcoefVectorNeutral, semDcoefVectorNeutral, 'Marker', 'o', 'Color', rgb('DarkGreen'), 'LineWidth', 2, 'MarkerFaceColor', 'w', 'MarkerSize', 10)
h1 = plot(1:3, dcoefVectorFix, 'k-', 'LineWidth', 2);
h2 = plot(1:3, dcoefVectorNeutral, 'Color',rgb('DarkGreen'), 'LineWidth', 2);
legend([h1 h2],{'Fixation','Task'},'Location','Best')
axis([.5 3.5 floor(min([dcoefVectorFix - semDcoefVectorFix,dcoefVectorNeutral - semDcoefVectorNeutral])), ceil(max([dcoefVectorFix + semDcoefVectorFix,dcoefVectorNeutral + semDcoefVectorNeutral]))])
set(gca, 'FontSize', 12, 'XTick', 1:3, 'XTickLabel', {'Dcoef','Dcoef X','Dcoef Y'})
ylabel('Diffusion Coefficient')
title(sprintf('%s Drift Dcoef', params.Subject{sub_idx}), 'FontSize', 15)
print('-djpeg',sprintf('%s/DriftDcoef/%s_%i.jpeg', jpegFilepath, params.Subject{sub_idx}, params.RGB(sub_idx)));
print('-depsc',sprintf('%s/DriftDcoef/%s_%i.epsc', epscFilepath, params.Subject{sub_idx}, params.RGB(sub_idx)));
print('-depsc',sprintf('%s/DriftDcoef_%s_%i.epsc', latexFilepath, params.Subject{sub_idx}, params.RGB(sub_idx)));

% Curvature
figure('position',[800 300 400 500])
curvVector = [ft.fixation.drift.avg_curvature, ft.neutral.drift.avg_curvature];
semCurvVector = [ft.fixation.drift.sem_curvature, ft.neutral.drift.sem_curvature];
errorbar(1:2, curvVector, semCurvVector, 'ok', 'LineWidth', 2, 'MarkerFaceColor', 'w', 'MarkerSize', 10)
hold on 
plot(1:2, curvVector, 'k-','LineWidth', 2)
axis([.5 2.5 floor(min(curvVector)), ceil(max(curvVector))])
set(gca, 'FontSize', 12, 'XTick', 1:3, 'XTickLabel', {'Fixation','Task'})
ylabel('Curvature')
title(sprintf('%s Drift Curvature', params.Subject{sub_idx}), 'FontSize', 15)
print('-djpeg',sprintf('%s/DriftCurv/%s_%i.jpeg', jpegFilepath, params.Subject{sub_idx}, params.RGB(sub_idx)));
print('-depsc',sprintf('%s/DriftCurv/%s_%i.epsc', epscFilepath, params.Subject{sub_idx}, params.RGB(sub_idx)));
print('-depsc',sprintf('%s/DriftCurv_%s_%i.epsc', latexFilepath, params.Subject{sub_idx}, params.RGB(sub_idx)));

% Speed
figure('position',[800 300 800 500])
velVectorFix = [ft.fixation.drift.avg_speed, ft.fixation.drift.avg_vel_x, ft.fixation.drift.avg_vel_y];
semVelVectorFix = [ft.fixation.drift.sem_speed, ft.fixation.drift.sem_vel_x, ft.fixation.drift.sem_vel_y];
velVectorNeutral = [ft.neutral.drift.avg_speed, ft.neutral.drift.avg_vel_x, ft.neutral.drift.avg_vel_y];
semVelVectorNeutral = [ft.neutral.drift.sem_speed, ft.neutral.drift.sem_vel_x, ft.neutral.drift.sem_vel_y];
errorbar(1:3, velVectorFix, semVelVectorFix, 'ok', 'LineWidth', 2, 'MarkerFaceColor', 'w', 'MarkerSize', 10)
hold on
errorbar(1:3, velVectorNeutral, semVelVectorNeutral, 'o', 'Color', rgb('DarkGreen'), 'LineWidth', 2)
h1 = plot(1:3, velVectorFix, 'k-', 'LineWidth', 2);
h2 = plot(1:3, velVectorNeutral, 'Color',rgb('DarkGreen'), 'LineWidth', 2);
legend([h1 h2],{'Fixation','Task'},'Location','Best')
axis([.5 3.5 floor(min([velVectorFix,velVectorNeutral])), ceil(max([velVectorFix,velVectorNeutral]))])
set(gca, 'FontSize', 12, 'XTick', 1:3, 'XTickLabel', {'Speed','Vel X','Vel Y'})
ylabel('Speed [arcmin/s]')
title(sprintf('%s Drift Velocity', params.Subject{sub_idx}), 'FontSize', 15)
print('-djpeg',sprintf('%s/DriftVel/%s_%i.jpeg', jpegFilepath, params.Subject{sub_idx}, params.RGB(sub_idx)));
print('-depsc',sprintf('%s/DriftVel/%s_%i.epsc', epscFilepath, params.Subject{sub_idx}, params.RGB(sub_idx)));
print('-depsc',sprintf('%s/DriftVel_%s_%i.epsc', latexFilepath, params.Subject{sub_idx}, params.RGB(sub_idx)));

% Span
figure('position',[800 300 800 500])
spanVector = [ft.fixation.drift.avg_span, ft.neutral.drift.avg_span];
semSpanVector = [ft.fixation.drift.sem_span, ft.neutral.drift.sem_span];
errorbar(1:2, spanVector, semSpanVector, 'ok', 'LineWidth', 2, 'MarkerFaceColor', 'w', 'MarkerSize', 10)
hold on 
plot(1:2, spanVector, 'k-','LineWidth', 2)
axis([.5 2.5 floor(min(spanVector)), ceil(max(spanVector))])
set(gca, 'FontSize', 12, 'XTick', 1:2, 'XTickLabel', {'Fixation','Task'})
ylabel('Span [arcmin]')
title(sprintf('%s Drift Span', params.Subject{sub_idx}), 'FontSize', 15)
print('-djpeg',sprintf('%s/DriftSpan/%s_%i.jpeg', jpegFilepath, params.Subject{sub_idx}, params.RGB(sub_idx)));
print('-depsc',sprintf('%s/DriftSpan/%s_%i.epsc', epscFilepath, params.Subject{sub_idx}, params.RGB(sub_idx)));
print('-depsc',sprintf('%s/DriftSpan_%s_%i.epsc', latexFilepath, params.Subject{sub_idx}, params.RGB(sub_idx)));

% Variance
figure('position',[800 300 800 500])
varVectorFix = [ft.fixation.drift.avg_varx, ft.fixation.drift.avg_vary];
semVarVectorFix = [ft.fixation.drift.sem_varx, ft.fixation.drift.sem_vary];
varVectorNeutral = [ft.neutral.drift.avg_varx, ft.neutral.drift.avg_vary];
semVarVectorNeutral = [ft.neutral.drift.sem_varx, ft.neutral.drift.sem_vary];
errorbar(1:2, varVectorFix, semVarVectorFix, 'ok', 'LineWidth', 2, 'MarkerFaceColor', 'w', 'MarkerSize', 10)
hold on
errorbar(1:2, varVectorNeutral, semVarVectorNeutral, 'o', 'Color', rgb('DarkGreen'), 'LineWidth', 2, 'MarkerFaceColor', 'w', 'MarkerSize', 10)
h1 = plot(1:2, varVectorFix, 'k-', 'LineWidth', 2);
h2 = plot(1:2, varVectorNeutral, 'Color',rgb('DarkGreen'), 'LineWidth', 2);
legend([h1 h2],{'Fixation','Task'},'Location','Best')
axis([.5 2.5 floor(min([varVectorFix,varVectorNeutral])), ceil(max([varVectorFix,varVectorNeutral]))])
set(gca, 'FontSize', 12, 'XTick', 1:2, 'XTickLabel', {'Speed','Vel X','Vel Y'})
ylabel('Speed [arcmin/s]')
title(sprintf('%s Drift Velocity', params.Subject{sub_idx}), 'FontSize', 15)
print('-djpeg',sprintf('%s/DriftVar/%s_%i.jpeg', jpegFilepath, params.Subject{sub_idx}, params.RGB(sub_idx)));
print('-depsc',sprintf('%s/DriftVar/%s_%i.epsc', epscFilepath, params.Subject{sub_idx}, params.RGB(sub_idx)));
print('-depsc',sprintf('%s/DriftVar_%s_%i.epsc', latexFilepath, params.Subject{sub_idx}, params.RGB(sub_idx)));


%% Landing Position and Performance Correlation
% figure('Position', [900 350 500 500])
% [t p b R] = LinRegression([0, 16, 29, 38, 41], ...
%     [ft.minus_0.avg_perf, ft.minus_1.avg_perf, ...
%     ft.minus_2.avg_perf, ft.minus_3.avg_perf, ft.minus_4.avg_perf], ...
%      0, NaN, 1, 0);
% axis([-5 45 0 1])
% set(gca, 'FontSize',12, 'XTick',[0, 16, 29, 38, 41], 'YTick',(0:.1:1))
% xlabel('Distance from cued location')
% ylabel('Proportion Correct')
% title({'Performance vs Distance'; sprintf('r = %.2f, p = %.2f b = %.2f', R, p, b)})
% print('-djpeg',sprintf('./Documents/Figures/%s_%i_CorrelationDistance.jpeg', params.Subject{sub_idx}, params.RGB(sub_idx)));
% 
% figure('Position', [900 350 500 500])
% [bin, nn, sens_thresh, ~] = PartitionEqualGroups_NS(ft.landing_error', 5, floor(min(ft.landing_error)), []);
% unique_bins = unique(bin);
% for bin_idx = 1:length(unique_bins)
%     avg_landing_error(bin_idx) = nanmean(ft.landing_error(bin == unique_bins(bin_idx)));
%     avg_performance(bin_idx) = nanmean(ft.performance(bin == unique_bins(bin_idx)));
% end
% [t p b R] = LinRegression(avg_landing_error, avg_performance, 0, NaN, 1, 0);
% axis([-5 45 0 1])
% set(gca, 'FontSize',12, 'XTick', (-5:5:45), 'YTick',(0:.1:1))
% xlabel('Landing Error')
% ylabel('Proportion Correct')
% title({'Performance vs Landing Error'; sprintf('r = %.2f, p = %.2f b = %.2f', R, p, b)})
% print('-djpeg',sprintf('./Documents/Figures/%s_%i_CorrelationLandingError.jpeg', params.Subject{sub_idx}, params.RGB(sub_idx)));

