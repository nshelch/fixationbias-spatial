diary(sprintf('%s_RGB%i_summary', params.Subject{sub_idx}, params.RGB(sub_idx)))
if params.DDPI
    diary(sprintf('%s_RGB%i_DDPI_summary', params.Subject{sub_idx}, params.RGB(sub_idx)))
end

diary ON
fprintf('Subject: %s\n', params.Subject{sub_idx})

totalNeutral = length([ft.neutral_1.id, ft.neutral_2.id, ft.neutral_3.id, ft.neutral_4.id, ft.neutral_5.id, ft.neutral_6.id, ft.neutral_7.id, ft.neutral_8.id, ft.neutral_9.id]);

% Peripheral condition summary
fprintf('\nProportion of selected trials: %.2f\n\n', totalNeutral/cont.filtered_trials)
fprintf('Performance (Neutral): %.2f (n = %d) d prime: %.2f \n', ft.neutral.avg_perf, cont.neutral, stats.d_prime.d.neutral)

% Bias during neutral condition
fprintf('\nPerformance (Neutral 1): %.2f (n = %d) d prime: %.2f \n', ft.neutral_1.avg_perf, cont.neutral_1, stats.d_prime.d.neutral_1)
fprintf('Performance (Neutral 2): %.2f (n = %d) d prime: %.2f \n', ft.neutral_2.avg_perf, cont.neutral_2, stats.d_prime.d.neutral_2)
fprintf('Performance (Neutral 3): %.2f (n = %d) d prime: %.2f \n', ft.neutral_3.avg_perf, cont.neutral_3, stats.d_prime.d.neutral_3)
fprintf('Performance (Neutral 4): %.2f (n = %d) d prime: %.2f \n', ft.neutral_4.avg_perf, cont.neutral_4, stats.d_prime.d.neutral_4)
fprintf('Performance (Neutral 5): %.2f (n = %d) d prime: %.2f \n', ft.neutral_5.avg_perf, cont.neutral_5, stats.d_prime.d.neutral_5)
fprintf('Performance (Neutral 6): %.2f (n = %d) d prime: %.2f \n', ft.neutral_6.avg_perf, cont.neutral_6, stats.d_prime.d.neutral_6)
fprintf('Performance (Neutral 7): %.2f (n = %d) d prime: %.2f \n', ft.neutral_7.avg_perf, cont.neutral_7, stats.d_prime.d.neutral_7)
fprintf('Performance (Neutral 8): %.2f (n = %d) d prime: %.2f \n', ft.neutral_8.avg_perf, cont.neutral_8, stats.d_prime.d.neutral_8)
fprintf('Performance (Neutral 9): %.2f (n = %d) d prime: %.2f \n', ft.neutral_9.avg_perf, cont.neutral_9, stats.d_prime.d.neutral_9)

% Checking trial counts
fprintf('\n Trials discarded for blinks: %d', dt.blinks)
fprintf('\n Trials discarded for no track: %d', dt.no_track)
fprintf('\n Trials discarded for no response: %d', dt.no_response)
fprintf('\n Trials discarded for trial duration: %d', dt.short_trial)
fprintf('\n Trials discarded for saccades: %d', dt.saccades)
fprintf('\n Trials discarded for microsaccades: %d', dt.microsaccades)
fprintf('\n Trials discarded for early microsaccades: %d', dt.early_ms)
fprintf('\n Trials discaded for gaze off center: %d', dt.gaze_off_center)
fprintf('\n Trials discaded manually: %d', dt.manual_discard)
fprintf('\n Number of valid trials: %d', cont.valid_trials)

fprintf('\n\n Trials prior to filter: %d', ii)
fprintf('\n Total trials: %d \n', cont.valid_trials + sum(cell2mat(struct2cell(dt))))

diary OFF

if params.DDPI
    movefile(sprintf('%s_RGB%i_DDPI_summary', params.Subject{sub_idx}, params.RGB(sub_idx)), './Summaries')
else
    movefile(sprintf('%s_RGB%i_summary', params.Subject{sub_idx}, params.RGB(sub_idx)), './Summaries')
end
