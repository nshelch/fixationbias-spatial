clear; clc; close all;

% Trial Overview:
%   DelayTime: the duration after trial starts before fixation time starts
%   FixationTime: the duration of the fixation time
%   BeepDelayTime: the variable duration before the saccade cue is presented
%   SaccCueTime: the duration of the saccade cue presentation
%   TargetTime: the duration of the target presentation
%   MaskTime: the duration of the mask after the target appeared
%   CueTime: the duration of the cue presentation

%   Z073: 90
%   Z031: 75
%   Z042: 75
%   Z095: 50

% All Subjects: {'Z073','Z042','Z031','Z066'}; [90, 75, 75, 50];

params.Subject = {'Z042'};
params.RGB = 75;
params.DDPI = 0;
params.FIX_FIGURES = 0;
params.TABLES = 0;
params.ACROSS_SUBJECT_TABLE = 0;

for sub_idx = 1:length(params.Subject)
    
    if params.DDPI
        load(sprintf('./Data/%s_RGB%i-DDPI.mat', params.Subject{sub_idx}, params.RGB(sub_idx)));
    else
        load(sprintf('./Data/%s_RGB%i.mat', params.Subject{sub_idx}, params.RGB(sub_idx)));
    end
    
    [pxAngle] = CalculatePxAngle(vt{1}.Xres, 1550, 600);
    DistThresh = 10;
    MaxMsAmp = 30;
    MinMsAmp = 0;
    
    if strcmp(params.Subject{sub_idx}, 'Z066')
        DistThresh = 18;
    end
    
    % Initialize Counters
    trial_types = {'neutral', 'other', 'neutral_1', 'neutral_2', ...
        'neutral_3', 'neutral_4', 'neutral_5', 'neutral_6', ...
        'neutral_7', 'neutral_8', 'neutral_9'};
    
    saved_data_filepath = sprintf('./Data/%s_RGB%i_summary.mat', params.Subject{sub_idx}, params.RGB(sub_idx));
    if params.DDPI
        saved_data_filepath = sprintf('./Data/%s_RGB%i_DDPI_summary.mat', params.Subject{sub_idx}, params.RGB(sub_idx));
    end
    
    for trial_idx = 1:length(trial_types)
        cur_trial = trial_types{trial_idx};
        ft.(cur_trial) = filteredTrialsStruct();
        cont.(cur_trial) = 0;
        heatmap.(cur_trial).xx(1) = NaN;
        heatmap.(cur_trial).yy(1) = NaN;
    end
    
    % Trial Statistics Structure
    dt = discardedTrialsStruct();
    cont.fix_ms_trial = 0;
    cont.fix_drift_trial = 0;
    cont.ms_trials = 0;
    cont.filtered_trials = 0;
    cont.valid_trials = 0;
    
    for ii = 1:length(vt)
        
        if vt{ii}.(sprintf('Target%iOrientation', vt{ii}.CueLocation)) == 135
            ft.orientation_cued_target(ii) = 0;
        elseif vt{ii}.(sprintf('Target%iOrientation', vt{ii}.CueLocation)) == 45
            ft.orientation_cued_target(ii) = 1;
        end
        
        % Finds all microsaccades that occured after saccade cue signal
        sacc_cue_on = vt{ii}.TimeFixationON + vt{ii}.FixationTime + vt{ii}.BeepDelayTime;
        idx_valid_ms = find(vt{ii}.microsaccades.start > sacc_cue_on);
        
        % first microsaccade after the saccade cue signal in all trials
        if ~isempty(idx_valid_ms)
            % ms_latency is a vector which tells how long it took the subject to
            % initiate a microsaccade
            ft.ms_initiation(ii) = (vt{ii}.microsaccades.start(idx_valid_ms(1)) - sacc_cue_on);
        end
        
        [ft, cont] = insert_fixation_data(ft, params, cont, vt, ii);
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        % eliminate trials with no tracks, blinks, large saccades, no response,
        % and too short trial duration
        [goodTrial, dt] = filter_trial(vt{ii}, dt, params.DDPI);
        
        if goodTrial
            
            % Trials in which the subject responded and passed a filter
            cont.filtered_trials = cont.filtered_trials + 1;
            sacc_cue_location = vt{ii}.SaccCueType;
            resp_cue_location = vt{ii}.CueLocation;
            
            % find the microsaccades/drifts performed 80 - 300 ms from target on
            % these two numbers can be parameters at the beginning of the code script
            target_on = vt{ii}.TimeTargetON;
            target_off = round(vt{ii}.TimeTargetON + vt{ii}.TargetTime);
            
            % check that the average gaze position when the target is on is
            % close to 0
            if params.DDPI
                start_time = round((target_on - 50) / (1000/330));
                end_time = round(target_off / (1000/330));
                xx_target = vt{ii}.x.position(start_time:end_time) + vt{ii}.xoffset * pxAngle;
                yy_target = vt{ii}.y.position(start_time:end_time) + vt{ii}.yoffset * pxAngle;
            else
                xx_target = vt{ii}.x.position(round(target_on - 50):target_off) + vt{ii}.xoffset * pxAngle;
                yy_target = vt{ii}.y.position(round(target_on - 50):target_off) + vt{ii}.yoffset * pxAngle;
            end
            
            % distance from the cued location
            dist_from_target = target_classification(xx_target, yy_target, resp_cue_location, double(vt{ii}.TargetOffsetpx), pxAngle);
            ft.dist_from_target_x(ii) = mean(xx_target); ft.dist_from_target_y(ii) = mean(yy_target);
            ft.avg_dist_from_target(ii) = mean(dist_from_target);
            ft.gaze_loc(cont.filtered_trials) = mean(sqrt(xx_target.^2 + yy_target.^2));
            
            % Plots the eye trace during target presentation
            %             plot_eye_trace(xx_target, yy_target, avg_dist_from_target(ii), Dist_thresh, sacc_cue_location, resp_cue_location, pxAngle)
            
            if mean(ft.gaze_loc(cont.filtered_trials)) < DistThresh
                [ft, cont, heatmap] = insert_trial_data(ft, params, cont, heatmap, vt, ii, xx_target, yy_target);
            else
                dt.gaze_off_center = dt.gaze_off_center + 1;
            end
        end
    end
    
    ft.response_time = cellfun(@(z) z(:).ResponseTime - z(:).TimeCueON - z(:).CueTime, vt);
    ft.sacc_cue_type = cellfun(@(z) z(:).SaccCueType, vt);
    ft.cue_location = cellfun(@(z) z(:).CueLocation, vt);
    ft.subj_resp = cellfun(@(z) z(:).Response, vt);
    ft.performance = cellfun(@(z) z(:).Correct, vt);
    ft.counter = cont;
    ft.heatmap = heatmap;
    
    for trial_idx = 1:length(trial_types)
        cur_trial = trial_types{trial_idx};
        if ~strcmp(cur_trial, 'fixation')
            ft.(cur_trial).avg_perf = sum(ft.(cur_trial).perf)/length(ft.(cur_trial).perf);
        end
        if cont.(cur_trial) ~= 0
            ft.(cur_trial).drift.avg_perf = sum(ft.(cur_trial).perf)/length(ft.(cur_trial).perf);
            ft.(cur_trial).drift.avg_curvature = nanmean(ft.(cur_trial).drift.curvature);
            ft.(cur_trial).drift.avg_speed = nanmean(ft.(cur_trial).drift.speed);
            ft.(cur_trial).drift.avg_span = nanmean(ft.(cur_trial).drift.span);
            ft.(cur_trial).avg_resp_time = nanmean(ft.(cur_trial).response_time);
            
            % Diffusion Coef
            [~,~, ft.(cur_trial).drift.dcoef, ~, ft.(cur_trial).drift.dsq, ft.(cur_trial).drift.singleSeg, ft.(cur_trial).drift.timeDsq, ~] = ...
                CalculateDiffusionCoef(ft.(cur_trial).drift.position);
            ft.(cur_trial).drift.indv_dcoef = getIndvDcoef(ft.(cur_trial).drift.singleSeg);
            ft.(cur_trial).drift.sem_dcoef = nansem(ft.(cur_trial).drift.indv_dcoef, length(ft.(cur_trial).drift.indv_dcoef));
            
            % Diffusion Coef in X
            [~,~, ft.(cur_trial).drift.dcoef_x, ~, ~, ft.(cur_trial).drift.singleSegX, ~, ~] = ...
                CalculateDiffusionCoef(ft.(cur_trial).drift.positionX);
            ft.(cur_trial).avg_dcoef_x = ft.(cur_trial).drift.dcoef_x/2;
            ft.(cur_trial).drift.indv_dcoef_x = (getIndvDcoef(ft.(cur_trial).drift.singleSegX) / 2);
            ft.(cur_trial).drift.sem_dcoef_x = nansem(ft.(cur_trial).drift.indv_dcoef_x, length(ft.(cur_trial).drift.indv_dcoef_x));
            
            % Diffusion Coef in Y
            [~,~, ft.(cur_trial).drift.dcoef_y, ~, ~, ft.(cur_trial).drift.singleSegY, ~, ~] = ...
                CalculateDiffusionCoef(ft.(cur_trial).drift.positionY);
            ft.(cur_trial).avg_dcoef_y = ft.(cur_trial).drift.dcoef_y/2;
            ft.(cur_trial).drift.indv_dcoef_y = (getIndvDcoef(ft.(cur_trial).drift.singleSegY) / 2);
            ft.(cur_trial).drift.sem_dcoef_y = nansem(ft.(cur_trial).drift.indv_dcoef_y, length(ft.(cur_trial).drift.indv_dcoef_y));
        end
    end
    
    ft.fixation.avg_ms_rate = nanmean(ft.fixation.ms_rate);
    
    stats = statisticalTests(ft, trial_types);
    if params.FIX_FIGURES
        FixationGraphs;
    end
    if params.TABLES
        generateTables; clc;
    end
    
    PrintSummaries
    save(saved_data_filepath, 'ft', 'stats', 'dt');
    
    if length(params.Subject) > 1
        close all; clc; clearvars -except sub_idx params
    end
end

if params.ACROSS_SUBJECT_TABLE
    
    params.Subject = {'Z073','Z031','Z066','Z042'};
    params.RGB = [90, 75, 50, 75];
    subRowNames = cell(1, length(params.Subject));
    for sub_idx = 1:length(params.Subject)
        load(sprintf('./Data/%s_RGB%i_summary.mat', params.Subject{sub_idx}, params.RGB(sub_idx)));
        trialStats(sub_idx,:) = [cell2mat(struct2cell(dt))', ft.counter.valid_trials, sub_idx];
        subRowNames{sub_idx} = sprintf('%s %i',params.Subject{sub_idx}, params.RGB(sub_idx));
    end
    
    tableParams.dataFormat = {'%i'};
    tableParams.completeTable = 1;
    tableParams.transposeTable = 0;
    tableParams.tableLabel = 'DiscardedTrials';
    tableParams.tableCaption = 'Trial Statistics';
    
    trialStruct = struct('Blinks', trialStats(:,1), ...
        'NoTracks', trialStats(:,2), ...
        'NoResp', trialStats(:,3), ...
        'TrialDur', trialStats(:,4), ...
        'Saccades', trialStats(:,5), ...
        'EarlyMs', trialStats(:,6), ...
        'LateMs', trialStats(:,7), ...
        'GazeOffCenter', trialStats(:,8), ...
        'Manual', trialStats(:,9), ...
        'ValidTrials', trialStats(:,10), ...
        'TotalTrials', trialStats(:,11));
    discardedTrialsTable = struct2table(trialStruct, 'rowNames', subRowNames);
    finalTableOutput(discardedTrialsTable, tableParams, './Documents/Peripheral Report', 'TrialStatistics.txt')
end
