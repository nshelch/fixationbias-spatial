% Saccade Cue

sacc_cue(1).x = [0, 0];
sacc_cue(1).y = [3.765, 8.765];

sacc_cue(2).x = [3.765, 8.765];
sacc_cue(2).y = [0, 0];

sacc_cue(3).x = [0, 0];
sacc_cue(3).y = [-3.765, -8.765];

sacc_cue(4).x = [-3.765, -8.765];
sacc_cue(4).y = [0, 0];
    
% Response Cue

resp_cue(1).x = [0, 0];
resp_cue(1).y = [24.3, 27.8];

resp_cue(2).x = [18.29, 20.79];
resp_cue(2).y = [18.29, 20.79];

resp_cue(3).x = [24.3, 27.8];
resp_cue(3).y = [0, 0];

resp_cue(4).x = [18.29, 20.79];
resp_cue(4).y = [-18.29, -20.79];

resp_cue(5).x = [0, 0];
resp_cue(5).y = [-24.3, -27.8];

resp_cue(6).x = [-18.29, -20.79];
resp_cue(6).y = [-18.29, -20.79];

resp_cue(7).x = [-24.3, -27.8];
resp_cue(7).y = [0, 0];

resp_cue(8).x = [-18.29, -20.79];
resp_cue(8).y = [18.29, 20.79];

resp_cue(9).x = [0, 0];
resp_cue(9).y = [3.765, 7.265];

% Box Locations
box_loc(1).topLeft = [-3.765, 24.3];
box_loc(1).topRight = [3.765, 24.3];
box_loc(1).bottomLeft = [-3.765, 16.77];
box_loc(1).bottomRight = [3.765, 16.77];

box_loc(2).topLeft = [10.76, 18.29];
box_loc(2).topRight = [18.29, 18.29];
box_loc(2).bottomLeft = [10.76, 10.76];
box_loc(2).bottomRight = [18.29, 10.76];

box_loc(3).topLeft = [16.77, 3.765];
box_loc(3).topRight = [24.3, 3.765];
box_loc(3).bottomLeft = [16.77, -3.765];
box_loc(3).bottomRight = [24.3, -3.765];

box_loc(4).topLeft = [10.76, -10.76];
box_loc(4).topRight = [18.29, -10.76];
box_loc(4).bottomLeft = [10.76, -18.29];
box_loc(4).bottomRight = [18.29, -18.29];

box_loc(5).topLeft = [-3.765, -16.77];
box_loc(5).topRight = [3.765, -16.77];
box_loc(5).bottomLeft = [-3.765, -24.3];
box_loc(5).bottomRight = [3.765, -24.3];

box_loc(6).topLeft = [-18.29, -10.76];
box_loc(6).topRight = [-10.76, -10.76];
box_loc(6).bottomLeft = [-18.29, -18.29];
box_loc(6).bottomRight = [-10.76, -18.29];

box_loc(7).topLeft = [-24.3, 3.765];
box_loc(7).topRight = [-16.77, 3.765];
box_loc(7).bottomLeft = [-24.3, -3.765];
box_loc(7).bottomRight = [-16.77, -3.765];

box_loc(8).topLeft = [-18.29, 18.29];
box_loc(8).topRight = [-10.76, 18.29];
box_loc(8).bottomLeft = [-18.29, 10.76];
box_loc(8).bottomRight = [-10.76, 10.76];

box_loc(9).topLeft = [-3.765, 3.765];
box_loc(9).topRight = [3.765, 3.765];
box_loc(9).bottomLeft = [-3.765, -3.765];
box_loc(9).bottomRight = [3.765, -3.765];