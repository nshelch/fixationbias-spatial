function vt = checkDrifts(drifts, trialId, vt)

figure('position',[200, 100, 1500, 800])
allDriftPlot = subplot(2, 2, 1);
plot(drifts.singleSeg')
xlim([5, 260])
box off; axis square
xlabel('Time interval [ms]','FontWeight','bold')
ylabel('dsq','FontWeight','bold')
title({'All drifts'; sprintf('Coef: %.2f', nanmean(drifts.dcoef))})
allDriftPlot.Position(1) = 0; % repositions the subplot on the x axis

PLOT_DRIFTS = input('Plot individual segments (y/n): ','s');

while ~(PLOT_DRIFTS  == 'y' || PLOT_DRIFTS == 'n')
    fprintf('Invalid input, try again')
    PLOT_DRIFTS = input('Plot individual segments (y/n): ','s');
end
    
if PLOT_DRIFTS == 'y'
    yLimit = input('What is the y limit: ');
    for driftIdx = 1:size(drifts.singleSeg, 1)
        indvDriftPlot = subplot(2, 2, 3);
        hold on
        currentTrialId = trialId(driftIdx);
        indvCoef = drifts.dcoef(driftIdx);
        plot(drifts.singleSeg(driftIdx,:)', 'r')
        title({sprintf('Trial: %i', currentTrialId); sprintf('Coef: %.2f \t Drift: %i/%i', indvCoef, driftIdx, size(drifts.singleSeg, 1))})
        xlabel('Time interval [ms]','FontWeight','bold')
        ylabel('dsq','FontWeight','bold')
        axis([5 260 0 yLimit])
        box off
        axis square
        indvDriftPlot.Position(1) = 0;
        
        trialPlot = subplot(2,2,2);
        trialPlot.Position = [0.35, 0.13, 0.6, 0.8];
        hold off % Turn hold off so that the individual traces don't plot on top of one another
        
        % Period of interest start and stop
        poiStart = vt{currentTrialId}.TimeFixationON + ...
            vt{currentTrialId}.FixationTime + ...
            vt{currentTrialId}.BeepDelayTime;
        poiEnd = vt{currentTrialId}.TimeTargetON + ...
            vt{currentTrialId}.TargetTime;
        
        % Creates a color block (green) which highlights the period of
        % interest
        
        poi = fill([poiStart, poiStart ...
            poiEnd, poiEnd], ...
            [-50, 50, 50, -50], ...
            'g', 'EdgeColor', 'g', 'LineWidth', 2, 'Clipping', 'On', 'FaceAlpha', 0.25); 
        xTrace = vt{currentTrialId}.x.position + vt{currentTrialId}.xoffset * .6848;
        yTrace = vt{currentTrialId}.y.position + vt{currentTrialId}.yoffset * .6848;
        hold on
        hx = plot(xTrace, 'Color', [0 0 200] / 255, 'HitTest', 'off', 'LineWidth', 2);
        hy = plot(yTrace, 'Color', [0 180 0] / 255, 'HitTest', 'off', 'LineWidth', 2);
        axis([poiStart - 400, poiEnd + 400, -35, 35])
        title('Trial Event Sequence')
        xlabel('Time','FontWeight','bold')
        ylabel('arcmin','FontWeight','bold')
        set(gca, 'FontSize', 12)
        legend([poi, hx, hy], {'P.O.I','X','Y'},'FontWeight','bold')
        
        cont = input('Stop (s) or discard the drift (d): ','s');
        if cont == 's'
            save(sprintf('./Data/%s.mat', vt{currentTrialId}.Subject_Name), 'vt')
            break
        elseif cont == 'd'
            invalidStartTime = input('What is the bad eye movement start time? ');
            vt{currentTrialId}.invalid.start = invalidStartTime;
        end
    end
end

save(sprintf('./Data/%s.mat', vt{1}.Subject_Name), 'vt')
close

end
