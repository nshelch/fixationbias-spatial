function [output, dt] = filter_trial(trial, dt, DDPI)

if check_blinks(trial)
    dt.blinks = dt.blinks + 1;
    output = 0;
elseif check_no_tracks(trial)
    dt.no_track = dt.no_track + 1;
    output = 0;
elseif ~check_trial_duration(trial, DDPI) % returns 0 if trial duration is short
    dt.short_trial = dt.short_trial + 1;
    output = 0;
elseif check_saccades(trial)
    dt.saccades = dt.saccades + 1;
    output = 0;
elseif check_no_response(trial)
    dt.no_response = dt.no_response + 1;
    output = 0;
elseif check_invalid(trial)
    dt.manual_discard = dt.manual_discard + 1;
    output = 0;
elseif check_microsaccades(trial)
    dt.microsaccades = dt.microsaccades + 1;
    output = 0;
elseif early_microsaccades(trial)
    dt.early_ms = dt.early_ms + 1;
    output = 0;
else
    output = 1;
end

end

function output = check_blinks(trial)
fix_on = trial.TimeFixationON;
target_on = trial.TimeTargetON;
output = isIntersectedIn( (fix_on - 100), (target_on + 400) - (fix_on - 100), trial.blinks);
end

function output = check_no_tracks(trial)
fix_on = trial.TimeFixationON;
target_on = trial.TimeTargetON;
output = isIntersectedIn( (fix_on - 100), (target_on + 400) - (fix_on - 100), trial.notracks);
end

function output = check_saccades(trial)
fix_on = trial.TimeFixationON;
resp_cue_on = trial.TimeCueON;
output = isIntersectedIn(fix_on, resp_cue_on - fix_on, trial.saccades);
end

function output = check_trial_duration(trial, DDPI)
if DDPI
    output = trial.TimeCueON < (length(trial.x.position) * (1000/330));
else
    output = trial.TimeCueON < length(trial.x.position);
end
end

function output = check_no_response(trial)
output = trial.Correct > 2;
end

function output = check_invalid(trial)
output = ~isempty(trial.invalid.start);
end

function output = check_microsaccades(trial)
% sacc_on = trial.TimeFixationON + trial.FixationTime + trial.BeepDelayTime;
resp_on = trial.TimeCueON;
target_on = trial.TimeTargetON;
output = isBetween(target_on - 100, trial.microsaccades.start, resp_on);
end

function output = early_microsaccades(trial)
sacc_on = trial.TimeFixationON + trial.FixationTime + trial.BeepDelayTime;
target_on = trial.TimeTargetON;
output = isBetween(sacc_on - 80, trial.microsaccades.start, target_on);
end