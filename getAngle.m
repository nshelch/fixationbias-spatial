function tmp = getAngle(x,y)

basisVec = [1,0];
% Calculates the angle from pi to -pi
tmp = acos(dot([x, y], basisVec) / (norm([x, y]) * norm(basisVec)));
% Changes it to be from 0 to 2*pi
if y < 0
    tmp = 2*pi - tmp;
end

end