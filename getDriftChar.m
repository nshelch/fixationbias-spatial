function [span, instSpX, instSpY, mn_speed, driftAngle, mn_cur, varx, vary] = getDriftChar(x, y, smoothing, cutseg, maxSpeed)
% smoothing: 41
% cutseg: how much of the drift do you wanna cut from the beginning and end
% maxSpeed (arcmin): 180

smx1 = sgfilt(x, 3, smoothing, 1);
smy1 = sgfilt(y, 3, smoothing, 1);
% compute drift speed
sp = 1000*sqrt(smx1(cutseg:end-cutseg).^2 + smy1(cutseg:end-cutseg).^2);

if any(sp > maxSpeed)
    span = nan; instSpX = nan; instSpY = nan; mn_speed= nan; driftAngle = nan; mn_cur = nan; varx = nan; vary = nan;
    
else
    % compute drift span
    mx = mean(x(cutseg:end-cutseg));
    my = mean(y(cutseg:end-cutseg));
    span = max(sqrt((x - mx).^2 + (y - my).^2));
    
    % s-golay filtering
    smx = sgfilt(x, 3, smoothing, 0);
    smy = sgfilt(y, 3, smoothing, 0);
    instSpX = smx1*1000; instSpY = smy1*1000;
    mn_speed = mean(sp);
    
    % compute drift curvature
    tmpCur = abs(cur(smx(cutseg:end-cutseg), smy(cutseg:end-cutseg)));
    tmpCur(tmpCur > 300) = NaN;
    mn_cur = nanmean(tmpCur);
    
    % Variance in x and y
    varx = var(x(cutseg:end-cutseg));
    vary = var(y(cutseg:end-cutseg));
    
    driftAngle = arrayfun(@(x,y) getAngle(x,y), instSpX, instSpY);
    
end
end