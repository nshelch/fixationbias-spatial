function tmp = getIndvDcoef(drift)

tmp = NaN(1, size(drift, 1));
for ii = 1:size(drift, 1)
    tmp(ii) = drift(ii, find(~isnan(drift(ii,:)), 1, 'last'));
end

end