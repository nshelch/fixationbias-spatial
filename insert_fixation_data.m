function [dataStruct, cont] = insert_fixation_data(dataStruct, params, cont, vt, ii)

saccCueOn = double(round(vt{ii}.TimeFixationON + vt{ii}.FixationTime + vt{ii}.BeepDelayTime));
xOffset = vt{ii}.xoffset * 0.6848;
yOffset = vt{ii}.yoffset * 0.6848;

idxFixationMs = find(vt{ii}.microsaccades.start < saccCueOn & vt{ii}.microsaccades.start > 50);
if ~isempty(idxFixationMs)
    cont.fix_ms_trial = cont.fix_ms_trial + 1;
    dataStruct.fixation.ms_rate(ii) = (length(idxFixationMs) / saccCueOn) * 1000;
    if params.DDPI
        ms_start_x = vt{ii}.x.position(vt{ii}.microsaccades.startSample(idxFixationMs)) + xOffset;
        ms_start_y = vt{ii}.y.position(vt{ii}.microsaccades.startSample(idxFixationMs)) + yOffset;
        ms_end_x = vt{ii}.x.position(vt{ii}.microsaccades.startSample(idxFixationMs) + vt{ii}.microsaccades.durationSample(idxFixationMs)) + xOffset;
        ms_end_y = vt{ii}.y.position(vt{ii}.microsaccades.startSample(idxFixationMs) + vt{ii}.microsaccades.durationSample(idxFixationMs)) + yOffset;
    else
        ms_start_x = vt{ii}.x.position(vt{ii}.microsaccades.start(idxFixationMs)) + xOffset;
        ms_start_y = vt{ii}.y.position(vt{ii}.microsaccades.start(idxFixationMs)) + yOffset;
        ms_end_x = vt{ii}.x.position(vt{ii}.microsaccades.start(idxFixationMs) + vt{ii}.microsaccades.duration(idxFixationMs)) + xOffset;
        ms_end_y = vt{ii}.y.position(vt{ii}.microsaccades.start(idxFixationMs) + vt{ii}.microsaccades.duration(idxFixationMs)) + yOffset;
    end
    dataStruct.fixation.ms_start_loc{ii} = [ms_start_x ; ms_start_y];
    dataStruct.fixation.ms_end_loc{ii} = [ms_end_x ; ms_end_y];
    dataStruct.fixation.ms_angle{ii} = vt{ii}.microsaccades.angle(idxFixationMs);
end

% Drift
idxDrift = find(vt{ii}.drifts.start < saccCueOn);
driftStart = round(vt{ii}.drifts.start(idxDrift));
if ( driftStart + vt{ii}.drifts.duration(idxDrift) ) > saccCueOn
    driftEnd = round(saccCueOn);
else
    driftEnd = round(driftStart + vt{ii}.drifts.duration(idxDrift)) - 1;
end

if params.DDPI
    start_time = round(driftStart/(1000/330));
    end_time = round(driftEnd/(1000/330));
    x = vt{ii}.x.position(start_time:end_time) + xOffset;
    y = vt{ii}.y.position(start_time:end_time) + yOffset;
else
    x = vt{ii}.x.position(driftStart:driftEnd) + xOffset;
    y = vt{ii}.y.position(driftStart:driftEnd) + yOffset;
end
if length(x) > 150
    cont.fix_drift_trial = cont.fix_drift_trial + 1;
    dataStruct = insert_drift_data(dataStruct, 'fixation', cont.fix_drift_trial, x, y);
end
end
