function [dataStruct, counter, heatmap] = insert_trial_data(dataStruct, params, counter, heatmap, vt, ii, xx_target, yy_target)

saccCueOn = round(vt{ii}.TimeFixationON + vt{ii}.FixationTime + vt{ii}.BeepDelayTime);
targetOff = round(vt{ii}.TimeTargetON + vt{ii}.TargetTime);
targetOffset = double(vt{ii}.TargetOffsetpx);
respCueLoc = vt{ii}.CueLocation;
saccCueLoc = vt{ii}.SaccCueType;
xOffset = vt{ii}.xoffset * vt{ii}.pxAngle;
yOffset = vt{ii}.yoffset * vt{ii}.pxAngle;

% Valid trial counter
counter.valid_trials = counter.valid_trials + 1;
dataStruct.valid_trial_id(counter.valid_trials) = ii;

% Getting the distance of gaze from the target during probe presentation
distFromTarget = target_classification(xx_target, yy_target, respCueLoc, targetOffset, vt{ii}.pxAngle);

% Heatmap for position of gaze during probe presentation separated based on
% the saccade cue
% heatmap.(sprintf('sacc_cue_%i_target', saccCueLoc)).xx = [heatmap.(sprintf('sacc_cue_%i_target', saccCueLoc)).xx, xx_target];
% heatmap.(sprintf('sacc_cue_%i_target', saccCueLoc)).yy = [heatmap.(sprintf('sacc_cue_%i_target', saccCueLoc)).yy, yy_target];

[trialType, counter] = trial_type_classification(respCueLoc, saccCueLoc, counter);

if contains(trialType, 'neutral') && ~strcmp(trialType, 'neutral_9')
    trialType = {trialType, 'neutral'};
    counter.neutral = counter.neutral + 1;
elseif strcmp(trialType, 'neutral_9')
    trialType = {'neutral_9'};
end

for curTrial = trialType
    trialCount = counter.(curTrial{1});
    dataStruct.(curTrial{1}).perf(trialCount) = vt{ii}.Correct;
    dataStruct.(curTrial{1}).id(trialCount) = ii;
    dataStruct.(curTrial{1}).gaze_pos(trialCount) = mean(sqrt(xx_target.^2 + yy_target.^2));
    dataStruct.(curTrial{1}).dist_from_target{trialCount} = distFromTarget;
    dataStruct.(curTrial{1}).response_time(trialCount) = vt{ii}.ResponseTime - vt{ii}.TimeCueON; % Response Time - Time Response Cue ON
    dataStruct.(curTrial{1}).subj_resp(trialCount) = vt{ii}.Response;
    
    if vt{ii}.(sprintf('Target%iOrientation', vt{ii}.CueLocation)) == 135
        dataStruct.(curTrial{1}).orientation_cued_target(trialCount) = 0;
    elseif vt{ii}.(sprintf('Target%iOrientation', vt{ii}.CueLocation)) == 45
        dataStruct.(curTrial{1}).orientation_cued_target(trialCount) = 1;
    end
    
    % Drift
    if params.DDPI
        start_time = round(saccCueOn/(1000/330));
        end_time = round(targetOff/(1000/330));
        x = vt{ii}.x.position(start_time:end_time) + xOffset;
        y = vt{ii}.y.position(start_time:end_time) + yOffset;
    else
        x = vt{ii}.x.position(round(saccCueOn):round(targetOff)) + xOffset;
        y = vt{ii}.y.position(round(saccCueOn):round(targetOff)) + yOffset;
    end
    
    dataStruct = insert_drift_data(dataStruct, curTrial{1}, trialCount, x, y);   
end

%         saccLandingTimeInterval = msEnd(msId):msEnd(msId) + 50; %msEnd(msId) - 25:msEnd(msId) + 25;
%         heatmap.sacc_cue_0_landing.xx = [heatmap.sacc_cue_0_landing.xx, ...
%             vt{ii}.x.position(saccLandingTimeInterval)+ xOffset];
%         heatmap.sacc_cue_0_landing.yy = [heatmap.sacc_cue_0_landing.yy, ...
%             vt{ii}.y.position(saccLandingTimeInterval)+ yOffset];

end


