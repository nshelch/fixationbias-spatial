function output = isBetween(lowerLimit, middleValue, upperLimit)

output = find(middleValue > lowerLimit & middleValue < upperLimit);

end