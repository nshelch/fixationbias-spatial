function y = nansem(data, n)

y = nanstd(data)/sqrt(n);

end

