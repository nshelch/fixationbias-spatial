function plotDistFromTarget(ft)
% Looks at the gaze location during target presentation and plot the
% distance from the target grouped by response cue

valid_trials = [ft.neutral_1.id, ft.neutral_2.id, ft.neutral_3.id, ...
    ft.neutral_4.id, ft.neutral_5.id, ft.neutral_6.id, ...
    ft.neutral_7.id, ft.neutral_8.id, ft.neutral_9.id];

for resp_cue = 1:9
    overlap = intersect(find(ft.cue_location == resp_cue), valid_trials);
    dist_from_target_resp{resp_cue} = ft.avg_dist_from_target(overlap);   
end

figure;
MultipleHist(dist_from_target_resp, 'binfactor', 1.1, 'samebins','smooth','color','parula');
legend({'1','2','3','4','5','6','7','8','9'})
title('Distance From Target During Probe Presentation')
ylabel('Probability')
xlabel('Distance from target [arcmin]')

end