function plotDprimeHeatmap(dprimeVector, titleStr)
sensitivityMatrix = zeros(19, 19);
rowLoc = [4, 6, 10, 14, 16, 14, 10, 6, 10];
colLoc = [10, 14, 16, 14, 10, 6, 4, 6, 10];

for boxLoc = 1:9
    row = rowLoc(boxLoc);
    col = colLoc(boxLoc);
    curDprime = dprimeVector(boxLoc);
    sensitivityMatrix(row, col) = curDprime;
    sensitivityMatrix(row + 1, col) = curDprime/2;
    sensitivityMatrix(row - 1, col) = curDprime/2;
    sensitivityMatrix(row, col - 1) = curDprime/2;
    sensitivityMatrix(row, col + 1) = curDprime/2;
    sensitivityMatrix(row - 1, col - 1) = curDprime/4;
    sensitivityMatrix(row - 1, col + 1) = curDprime/4;
    sensitivityMatrix(row + 1, col - 1) = curDprime/4;
    sensitivityMatrix(row + 1, col + 1) = curDprime/4;
end

sensitivityMatrix(isnan(sensitivityMatrix) | isinf(sensitivityMatrix)) = 0;
if min(min(sensitivityMatrix)) < 0
    sensitivityMatrix(sensitivityMatrix == 0) = -.5;
end
sensitivityMatrix = imgaussfilt(sensitivityMatrix, 1);
figure;
load('./MyColormaps.mat')
set(gcf, 'Colormap', mycmap)
pcolor(flipud(sensitivityMatrix)); axis square; shading interp;
cbh = colorbar;
cbh.Limits = [floor(min(dprimeVector)), ceil(max(max(sensitivityMatrix)))];
ylabel(cbh, "Sensitivity [d']")
% cbh.TickLabels = {(minDprime:0.2:maxDprime)'};
axis on
xlabel('Arcmin')
ylabel('Arcmin')
set(gca, 'FontSize', 15, 'xtick', 1:3:40, 'ytick', 1:3:40)
set(gca, 'xticklabels',{-30:10:30}, 'yticklabels',{-30:10:30})
axis square
axis tight
box off
title(titleStr)
end