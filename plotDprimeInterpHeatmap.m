function plotDprimeInterpHeatmap(dprimeVector, titleStr)

sensitivityMatrix = zeros(19, 19);
rowLoc = [4, 6, 10, 14, 16, 14, 10, 6, 10];
colLoc = [10, 14, 16, 14, 10, 6, 4, 6, 10];

for boxLoc = 1:9
    row = rowLoc(boxLoc);
    col = colLoc(boxLoc);
    curDprime = dprimeVector(boxLoc);
    sensitivityMatrix(row, col) = curDprime;
    sensitivityMatrix(row + 1, col) = curDprime/2;
    sensitivityMatrix(row - 1, col) = curDprime/2;
    sensitivityMatrix(row, col - 1) = curDprime/2;
    sensitivityMatrix(row, col + 1) = curDprime/2; 
    sensitivityMatrix(row - 1, col - 1) = curDprime/4;
    sensitivityMatrix(row - 1, col + 1) = curDprime/4;
    sensitivityMatrix(row + 1, col - 1) = curDprime/4;
    sensitivityMatrix(row + 1, col + 1) = curDprime/4;
end

[xi, yi] = meshgrid(2:.5:18, 2:.5:18);
zi = griddata(colLoc, rowLoc, dprimeVector, xi, yi);
xr = reshape(xi, [1089,1]);
yr = reshape(yi, [1089,1]);
zr = reshape(zi, [1089,1]);
F = scatteredInterpolant([xr, yr], zr);
F.Method = 'natural';
F.ExtrapolationMethod = 'nearest';
zr = F(xi,yi);

figure;
load('./MyColormaps.mat')
set(gcf, 'Colormap', mycmap)
pcolor(-32:2:32, -32:2:32, flipud(zr))
xlabel('Arcmin'); ylabel('Arcmin')
set(gca, 'FontSize', 15)
set(gca, 'xtick', -24:8:24, 'ytick', -24:8:24)
set(gca, 'xticklabels',{-30:10:30}, 'yticklabels',{-30:10:30})
set(gca, 'FontSize', 15)
axis square tight
shading interp
box off
title(titleStr)
colorbar
axis([-25 25 -25 25])

end
