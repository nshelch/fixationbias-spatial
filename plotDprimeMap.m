function plotDprimeMap(dprimeVector, counter, titleStr)

% dprimeVector (1x9 vector): a vector containing the dprime values for the specific
% condition 
% counter (1x9 vector): a vector containing the number of trials for the specific
% condition 

% Gets the range of dprime values and creates a colormap which corresponds
% to this range
minDprime = floor(min(dprimeVector));
if max(dprimeVector) <= 2
    maxDprime = ceil(max(dprimeVector));
else
    sortedDprime = sort(dprimeVector(~isnan(dprimeVector)));
    maxDprime = ceil(sortedDprime(end - 1));
end
dprimeRange = round(minDprime:.01:maxDprime, 2);
cmapRange = colormap(parula(length(dprimeRange)));

% Box Locations                     Box Corner Order for Fill
%           1                           
%       8       2                       4---------3
%   7       9       3                             |  
%       6       4                                 |  
%           5                                     |  
%                                       1---------2

box_locations;
close;
figure('position', [500, 300, 725, 600]);
hold on;

for boxId = 1:9
    x_corners = [box_loc(boxId).bottomLeft(1), box_loc(boxId).bottomRight(1), box_loc(boxId).topRight(1), box_loc(boxId).topLeft(1)];
    y_corners = [box_loc(boxId).bottomLeft(2), box_loc(boxId).bottomRight(2), box_loc(boxId).topRight(2), box_loc(boxId).topLeft(2)];
    
    if ~isnan(dprimeVector(boxId)) && ~isempty(find(round(dprimeVector(boxId), 2) == dprimeRange, 1))
        boxId = find(round(dprimeVector(boxId), 2) == dprimeRange);
        boxColor = cmapRange(boxId, :);
    elseif isempty(find(round(dprimeVector(boxId), 2) == dprimeRange, 1)) && dprimeVector(boxId) > maxDprime
        boxId = size(cmapRange, 1);
        boxColor = cmapRange(boxId, :);
    elseif isnan(dprimeVector(boxId))
        boxColor = 'w';
    end
     fill(x_corners, y_corners, boxColor)
end

colormap parula
cbh = colorbar;
ylabel(cbh, sprintf("max D' saturated and set to %i", maxDprime))
cbh.Ticks = linspace(0,1,length(minDprime:0.2:maxDprime));
cbh.TickLabels = {(minDprime:0.2:maxDprime)'};
axis([-30 30 -30 30])
xlabel('Arcmin')
ylabel('Arcmin')
title(titleStr)
set(gca, 'FontSize', 15)
axis square
box off

    text(-3.5, 15, sprintf('d'' = %0.2f', dprimeVector(1)), 'FontSize', 12)
    
    text(11, 9, sprintf('d'' = %0.2f', dprimeVector(2)), 'FontSize', 12)
    
    text(17, -5.5, sprintf('d'' = %0.2f', dprimeVector(3)), 'FontSize', 12)
    
    text(11, -20, sprintf('d'' = %0.2f', dprimeVector(4)), 'FontSize', 12)
    
    text(-3.5, -26, sprintf('d'' = %0.2f', dprimeVector(5)), 'FontSize', 12)
    
    text(-18, -20, sprintf('d'' = %0.2f', dprimeVector(6)), 'FontSize', 12)
    
    text(-24, -5.5, sprintf('d'' = %0.2f', dprimeVector(7)), 'FontSize', 12)
    
    text(-18, 9, sprintf('d'' = %0.2f', dprimeVector(8)), 'FontSize', 12)
    
    text(-3.5, -5.5, sprintf('d'' = %0.2f', dprimeVector(9)), 'FontSize', 12)
    
if ~isempty(counter)
    text(-2.5, 26, sprintf('n = %i', counter(1)), 'FontSize', 12)
    
    text(12, 20, sprintf('n = %i', counter(2)), 'FontSize', 12)
    
    text(18, 5.5, sprintf('n = %i', counter(3)), 'FontSize', 12)
    
    text(12, -9, sprintf('n = %i', counter(4)), 'FontSize', 12)
    
    text(-2.5, -15, sprintf('n = %i', counter(5)), 'FontSize', 12)
    
    text(-17, -9, sprintf('n = %i', counter(6)), 'FontSize', 12)
    
    text(-23, 5.5, sprintf('n = %i', counter(7)), 'FontSize', 12)
    
    text(-17, 20, sprintf('n = %i', counter(8)), 'FontSize', 12)
    
    text(-2.5, 5.5, sprintf('n = %i', counter(9)), 'FontSize', 12)
end

end