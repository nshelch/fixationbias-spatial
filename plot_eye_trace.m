function plot_eye_trace(xx, yy, distFromTarget, distThresh, saccCueLoc, respCueLoc, pxAngle)

plot_stimuli_location(0, 11*pxAngle, 30*pxAngle)
hold on
viscircles([0, 0], distThresh, 'Color','r')
target_xy = plot(xx, yy);
title(sprintf('Distance from target: %.2f\n Sacc Cue: %i  Resp Cue:  %i', ...
    distFromTarget, saccCueLoc, respCueLoc))
input ''
delete(target_xy)

end