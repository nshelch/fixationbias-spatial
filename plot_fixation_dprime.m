function plot_fixation_dprime(v_dprime, ci_dprime, titleStr, neutralConditions)

errorbar(v_dprime, ci_dprime, 'ok','LineStyle', 'none', 'LineWidth',2)
hold on
title(titleStr)
set(gca, 'FontSize', 15, 'XTick', 1:2, 'XTickLabel', neutralConditions, ...
    'YTick', -10:1:10)
ylabel("Sensitivity (d')")
xlabel('Neutral Conditon')
try
    ylim([floor(nanmin(v_dprime) - nanmax(ci_dprime)), ceil(nanmax(v_dprime) + nanmax(ci_dprime))])
catch
    ylim([-1 1])
end

xlim([0.5 2.5])
box off

end