function plot_fixation_performance(v_perf, std_perf, titleStr, neutralConditions)

errorbar(v_perf, std_perf, 'ok','LineStyle', 'none', 'LineWidth',2)
title(titleStr)
set(gca, 'FontSize', 15, 'XTick', 1:2, 'XTickLabel', neutralConditions, ...
    'YTick', .3:.1:1)
ylabel('Proportion Correct')
xlabel('Neutral Conditon')
ylim([.3, 1])
xlim([0.5 2.5])
box off

end