function saveFigure(figureName, figureLocation, extension)
% figureName: name of the figure, should be constant
% figureLocation: can be one or more, in a cell
% extension: should be the same length as figure locations

for ii = 1:length(figureLocation)
    curExtension = sprintf('-d%s', extension{ii});
    if ~exist(figureLocation{ii}, 'dir')
       mkdir(figureLocation{ii})
    end
    print(curExtension, sprintf('%s/%s.%s', figureLocation{ii}, figureName{ii}, extension{ii}));
end

end