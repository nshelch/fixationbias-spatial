function [stats] = statisticalTests(ft, trialTypes)

for trial_idx = 1:length(trialTypes)
    cur_trial = trialTypes{trial_idx};
    if ~isnan(ft.(cur_trial).perf)
        [stats.d_prime.d.(cur_trial), ~, stats.d_prime.ci.(cur_trial), stats.d_prime.crit.(cur_trial), stats.d_prime.var.(cur_trial)] = ...
            CalculateDprime_2(ft.subj_resp(ft.(cur_trial).id), ft.orientation_cued_target(ft.(cur_trial).id));
    else
        [stats.d_prime.d.(cur_trial), stats.d_prime.ci.(cur_trial), ...
            stats.d_prime.crit.(cur_trial), stats.d_prime.var.(cur_trial)] = deal(NaN);
        
    end
end

end