function value = target_classification(x, y, location, targetOffset, pxAngle)

target_loc = targetOffset*pxAngle;
diag_target_loc = targetOffset*pxAngle*sqrt(2)/2;

switch location
    case 0 
        x1 = 0; y1 = 0;
        x2 = x; y2 = y;
    case 1
        x1 = 0; y1 = target_loc;
        x2 = x; y2 = y;
    case 2
        x1 = diag_target_loc; y1 = diag_target_loc;
        x2 = x; y2 = y;
    case 3
        x1 = target_loc; y1 = 0;
        x2 = x; y2 = y;
    case 4
        x1 = diag_target_loc; y1 = -diag_target_loc;
        x2 = x; y2 = y;
    case 5
        x1 = 0; y1 = -target_loc;
        x2 = x; y2 = y;
    case 6
        x1 = -diag_target_loc; y1 = -diag_target_loc;
        x2 = x; y2 = y;
    case 7
        x1 = -target_loc; y1 = 0;
        x2 = x; y2 = y;
    case 8
        x1 = -diag_target_loc; y1 = diag_target_loc;
        x2 = x; y2 = y;
    case 9
        x1 = 0; y1 = 0;
        x2 = x; y2 = y;
end

value = sqrt((x2 - x1).^2 + (y2 - y1).^2);

end

