function [trial_type, counter] = trial_type_classification(respCue, saccCue, counter)

trial_type = 'other';

% Neutral SaccCue + Peripheral RespCue
if saccCue == 0
    trial_type = sprintf('neutral_%i', respCue);
end

counter.(trial_type) = counter.(trial_type) + 1;

end